# Dockerfiles for amrex/warpx ci

To build :
```
docker build -f Dockerfile kitware/paraview:ci-catalyst-amrex-warpx-YYYYMMDD
```

`install_amrex.sh` and `install_warpx.sh` can be optionally included in the image to test unmerged amrex branches or for debugging in general.
