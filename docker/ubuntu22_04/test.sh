#!/usr/bin/env bash
set -e
set -x

export CATALYST_DEBUG=1
export CATALYST_IMPLEMENTATION_PATHS="/opt/paraview/lib/catalyst"
readonly Example_DIR="$HOME/warpx/src/Examples/Tests/electrostatic_sphere"
export PYTHONPATH="$Example_DIR"

/root/warpx/build/bin/warpx.3d "$Example_DIR/inputs_3d_catalyst"
