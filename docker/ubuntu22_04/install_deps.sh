#!/bin/sh

set -e

export DEBIAN_FRONTEND=noninteractive
apt-get update

# Install system dependencies
apt-get install -y --no-install-recommends \
    ca-certificates curl

# Install osmesa 
apt-get install -y --no-install-recommends \
    libosmesa6-dev

# Install Git requirements.
apt-get install -y --no-install-recommends \
    git

# Development tools
apt-get install -y --no-install-recommends \
    gcc g++ make ninja-build

# MPI support
apt-get install -y --no-install-recommends \
    mpich libmpich-dev

# Install python dependecies.
apt-get install -y --no-install-recommends \
    python3 libpython3-dev

# Remove unnecessary files
apt-get clean
