#!/usr/bin/env bash
set -e

export CMAKE_PREFIX_PATH="/opt/conduit;/opt/catalyst;/opt/amrex"

readonly warpx_repo="https://github.com/ChristosT/warpx"
readonly warpx_commit="catalyst2-support"

readonly warpx_root="$HOME/warpx"
readonly warpx_src="$warpx_root/src"
readonly warpx_build_root="$warpx_root/build"
readonly prefix="/opt/warpx"

git clone "$warpx_repo" "$warpx_src"
git -C "$warpx_src" checkout "$warpx_commit"

cmake -S "$warpx_src" -B "$warpx_build_root" \
-GNinja \
-DCMAKE_BUILD_TYPE=Debug         \
-DWarpX_amrex_internal=OFF       \
-DWarpX_DIMS=3                   \
-DCMAKE_INSTALL_PREFIX="$prefix" 

cmake --build "$warpx_build_root"
