#!/bin/sh
set -x
set -e
readonly paraview_repo="https://gitlab.kitware.com/paraview/paraview"
readonly paraview_commit="4ef351a54ff747ef7169e2e52e77d9703a9dfa77" # master as of 2024.07.01, we need paraview/paraview!6902 which is not part of a release yet.

readonly paraview_root="$HOME/paraview"
readonly paraview_src="$paraview_root/src"
readonly paraview_build_root="$paraview_root/build"
readonly paraview_prefix="/opt/paraview"

git clone "$paraview_repo" "$paraview_src"
git -C "$paraview_src" checkout "$paraview_commit"
git -C "$paraview_src" submodule sync --recursive
git -C "$paraview_src" submodule update --init --recursive

export CMAKE_PREFIX_PATH="/opt/conduit;/opt/catalyst"
cmake -GNinja \
        -S "$paraview_src" \
        -B "$paraview_build_root" \
        -DCMAKE_BUILD_TYPE="Release" \
        -DPARAVIEW_USE_PYTHON="ON" \
        -DPARAVIEW_USE_MPI="ON" \
        -DVTK_USE_X="OFF" \
        -DVTK_OPENGL_HAS_OSMESA="ON" \
        -DPARAVIEW_ENABLE_CATALYST="ON" \
        -DPARAVIEW_BUILD_ALL_MODULES="OFF" \
        -DPARAVIEW_BUILD_EDITION="CATALYST_RENDERING" \
        -DCMAKE_INSTALL_PREFIX="$paraview_prefix"

cmake --build "$paraview_build_root" --target install

rm -rf "$paraview_build_root"
