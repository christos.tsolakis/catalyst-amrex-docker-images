#!/usr/bin/env bash
set -e

export CMAKE_PREFIX_PATH="/opt/conduit;/opt/catalyst"

readonly amrex_repo="https://github.com/ChristosT/amrex"
readonly amrex_commit="catalyst-support"

readonly amrex_root="$HOME/amrex"
readonly amrex_src="$amrex_root/src"
readonly amrex_build_root="$amrex_root/build"
readonly prefix="/opt/amrex"

#git clone https://github.com/AMReX-Codes/amrex 
#git -C amrex fetch origin pull/3444/head:catalyst-support
git clone "$amrex_repo" "$amrex_src"
git -C "$amrex_src" checkout "$amrex_commit"

cmake -S "${amrex_src}" -B "${amrex_build_root}"  \
-GNinja                          \
-DCMAKE_BUILD_TYPE=Release       \
-DAMReX_ENABLE_TESTS=ON          \
-DAMReX_FORTRAN=OFF              \
-DAMReX_SPACEDIM="2;3"           \
-DAMReX_CATALYST=TRUE            \
-DAMReX_CONDUIT=TRUE             \
-DAMReX_MPI=TRUE                 \
-DAMReX_MPI_THREAD_MULTIPLE=TRUE \
-DAMReX_LINEAR_SOLVERS=TRUE      \
-DAMReX_PARTICLES=TRUE           \
-DAMReX_PARTICLES_PRECISION="DOUBLE" \
-DAMReX_PIC=TRUE                     \
-DAMReX_TINY_PROFILE=TRUE            \
-DCMAKE_INSTALL_PREFIX="$prefix" 

cmake --build "$amrex_build_root" --target install
