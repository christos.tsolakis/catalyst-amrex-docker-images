#!/bin/sh
set -x
set -e

readonly catalyst_repo="https://gitlab.kitware.com/paraview/catalyst"
readonly catalyst_commit="v2.0.0"

readonly catalyst_root="$HOME/catalyst"
readonly catalyst_src="$catalyst_root/src"
readonly catalyst_build_root="$catalyst_root/build"
readonly catalyst_prefix="/opt/catalyst"

git clone "$catalyst_repo" "$catalyst_src"
git -C "$catalyst_src" checkout "$catalyst_commit"


/opt/cmake/bin/cmake -GNinja \
    -S "$catalyst_src" \
    -B "$catalyst_build_root" \
    -DCATALYST_BUILD_SHARED_LIBS=ON \
    -DCATALYST_BUILD_TESTING=OFF \
    -DCMAKE_BUILD_TYPE=Debug \
    -DCATALYST_WRAP_PYTHON=OFF \
    -DCATALYST_WRAP_FORTRAN=OFF \
    -DCATALYST_USE_MPI=ON \
    -DCATALYST_WITH_EXTERNAL_CONDUIT=ON \
    -DConduit_DIR="/opt/conduit/lib/cmake/conduit" \
    -DCMAKE_INSTALL_PREFIX="$catalyst_prefix" 

/opt/cmake/bin/cmake --build "$catalyst_build_root" --target install

rm -rf "$catalyst_root"
